/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectexam;

import java.util.Arrays;

/**
 *
 * @author Kenneth Maloles
 */
public class ProjectExam {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n1=0,n2=1,n3,i,count=20;    
        System.out.print(n1+" "+n2);  
    
        for(i=2;i<count;i++)    
        {    
         n3=n1+n2;    
         System.out.print(" "+n3);    
         n1=n2;    
         n2=n3;    
        }    
        System.out.print("\n\n"); 
        
        int arr1[] = {2, 3, 4, 5, 500, 34, 50};
        ProjectExam(arr1);

        int middle = arr1.length/2;
        int medianValue = 0;
        if (arr1.length%2 == 1) 
            medianValue = arr1[middle];
        else
            medianValue = (arr1[middle-1] + arr1[middle]) / 2;
        System.out.print("Median Value: " +  medianValue);
        System.out.print("\n\n"); 
        
        int highest = 0;
        highest = arr1[arr1.length - 1];
        System.out.print("Highest Value: " +  highest);  
        System.out.print("\n\n");
        
    }
    static void ProjectExam(int[] arr) {  
        int n = arr.length;  
        int temp = 0;  
        for(int i=0; i < n; i++){  
            for(int j=1; j < (n-i); j++){  
                if(arr[j-1] > arr[j]){  
                    //swap elements  
                    temp = arr[j-1];  
                    arr[j-1] = arr[j];  
                    arr[j] = temp;  
                }  
            }
        }  
    }  
  
    
}


